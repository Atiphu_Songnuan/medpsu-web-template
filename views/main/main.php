<?php header('Content-Type: text/html; charset=utf-8');?>
<!DOCTYPE html>
<html lang="th" translate="no">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="Content-Language" content="th">
  <!-- <meta http-equiv="content-Type" content="text/html; charset=tis-620"> -->
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link rel="icon" type="image/png" href="public/img/delivery.png" />
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css" />
  <link rel="stylesheet" href="bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css" />
  <!-- <link rel="stylesheet" href="public/css/define.css">
  <link rel="stylesheet" href="public/css/but.css"> -->
  <!-- <link rel="stylesheet" href="public/css/styles.css" /> -->
  <link href="//cdn.jsdelivr.net/npm/@sweetalert2/theme-default@3/default.css" rel="stylesheet" />

  <title>Home Drugs</title>
  <script src="bower_components/jquery/dist/jquery.min.js"></script>
  <script src="https://kit.fontawesome.com/b7a24e7c0e.js" crossorigin="anonymous"></script>
  <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
  <script src="//cdn.jsdelivr.net/npm/sweetalert2@9/dist/sweetalert2.min.js"></script>

  <!-- <script src="public/js/qrcode.js"></script> -->
  <script src="public/js/scripts.min.js"></script>
  <script src="public/js/payment.min.js"></script>
  <script src="https://www.google.com/recaptcha/api.js"></script>

  <!-- *Datatable -->
  <script src="bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
  <!-- <script src="bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script> -->
  <script src="bower_components/datatables.net-select/js/dataTables.select.min.js"></script>

  <!-- QR Code Scanner -->
  <!-- <script src="public/js/html5-qrcode.min.js"></script> -->
  <!-- <script src="public/js/qrcode/filereader.js"></script>
  <script src="public/js/qrcode/qrcodelib.js"></script>
  <script src="public/js/qrcode/webcodecamjs.js"></script>
  <script src="public/js/qrcode/main.js"></script> -->


  <style>
    html {
      position: relative;
      min-height: 100%;
    }

    body {
      padding-top: 0;
      padding-left: 0;
      margin: 0;
      font-family: 'Niramit', sans-serif;
      background-color: #eee;
      margin-bottom: 60px;
    }

    label,
    span a,
    p,
    h1,
    h2,
    h3,
    h4,
    h5 {
      font-family: 'Niramit', sans-serif;
      /* font-size: 1.1em; */
      font-weight: 300;
      line-height: 1.7em;
      color: black;
    }

    .swal2-footer {
      text-align: center;
    }

    body.swal2-height-auto {
      height: 100% !important
    }

    .swal2-styled.swal2-cancel {
      font-family: 'Niramit', sans-serif;
    }

    th.dt-center,
    td.dt-center {
      text-align: center;
    }

    .footer {
      position: absolute;
      bottom: 0;
      width: 100%;
      background-color: #1e4b25;
    }

    body>.container {
      padding: 60px 15px 0;
    }

    .footer>.container {
      padding-right: 15px;
      padding-left: 15px;
    }

    .block {
      display: block;
      width: 100%;
      border: none;
      padding: 14px 28px;
      font-size: 16px;
      text-align: center;
    }


    @media only screen and (max-width: 768px) {
      .card {
        width: 100%;
      }

      .clinic-card {
        width: 100%;
      }

      .bg {
        height: auto;
      }
    }
  </style>
</head>

<body>
  <header>
    <!-- Fixed navbar -->
    <div class="navbar shadow-sm" style="background-color: #1e4b25;">
      <div class="container d-flex justify-content-between">
        <a class="navbar-brand text-white d-flex align-items-center">
          <img src="public/img/medpsu.png" width="50" height="60" class="d-inline-block align-top" alt="">
          <span class="ml-3">My Header</span>
        </a>
      </div>
    </div>
  </header>

  <!-- Begin page content -->
  <main role="main" class="p-3 container">
    <div class="card mx-auto">
      <div class="card-body">
        My Content
      </div>
    </div>
  </main>

  <footer class="p-2 text-center footer">
    <div class="container">
      <span class="text-white">ฝ่ายเทคโนโลยีสารสนเทศ คณะแพทยศาสตร์ <br />โรงพยาบาลสงขลานครินทร์</span>
    </div>
  </footer>
</body>
<script>
  let redirecturl = "<?php echo $_SERVER['REQUEST_URI']; ?>"
</script>

</html>