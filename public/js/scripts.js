let mobilephone = "";
let patientkey;
let wherelikeMobile;

// =====Input=====
let hn;
let idcard;
let birthdate;
// ===============

// Payment
let paymentrecheckinterval;
let paymentcheckmethod;
let selfpaymentrecheckinterval;
let paymentlist = [];
let homemoneylist = [];
let paymenttable;
let paymentRefID;
let totalPay = 0;
let invoiceidArr = [];
// ===============
let trackingtable;

let confirmaddressbody;
let allClinic;
let ckeckedList = [];
let confirmAction;
$(document).ready(function () {
  let checkURL = redirecturl.split("?");
  if (checkURL.length === 2) {
    window.close();
  }
  $("form").submit(function (event) {
    event.preventDefault();
  });

  $("#verifyModal").modal("show");

  $("#appointselect").change(function () {
    let curdateap = $("#appointselect option:selected").attr("dateap");
    let curdoctname = $("#appointselect option:selected").attr("doctname");
    $("#dateap").text(convertToDateTH(curdateap));
    if (curdoctname != "") {
      $("#doctname").text(curdoctname);
    } else {
      $("#doctname").text("-");
    }
  });
});

function verifyPatient(token) {
  let requestbody = {
    action: "captchaverify",
    data: {
      response: token,
    },
  };
  $.ajax({
    url: "libs/ApiService.php", //the page containing php script
    headers: {
      "Content-Type": "application/json; charset=utf-8",
    },
    type: "POST", //request type,
    data: JSON.stringify(requestbody),
    // data: jsondata,
    error: function (error) {
      console.log(error);
    },
    success: (recaptcharesponse) => {
      let recaptcha = JSON.parse(recaptcharesponse);
      if (recaptcha.success && recaptcha.score >= 0.1) {
        hn = $("#verifyInputHN").val();
        idcard = $("#verifyInputIDNumber").val();
        birthdate = $("#birthdateInputIDNumber").val();
        phone = $("#mobileInputIDNumber").val();
        if (phone != "") {
          phone =
            "%" +
            phone.substring(0, 3) +
            "%" +
            phone.substring(3, 6) +
            "%" +
            phone.substring(6, 11) +
            "%";
        }
        if (hn != "" && (idcard != "" || birthdate != "" || phone != "")) {
          if (
            hn.length === 7 &&
            (idcard.length === 13 ||
              birthdate.length === 10 ||
              phone.length === 14)
          ) {
            let body = {
              action: "verifypatient",
              data: {
                hn: hn,
                idcard: idcard,
                birthdate: birthdate,
                phone: phone,
              },
            };
            $.ajax({
              url: "libs/ApiService.php", //the page containing php script
              headers: {
                "Content-Type": "application/json; charset=utf-8",
              },
              type: "POST", //request type,
              data: JSON.stringify(body),
              // data: jsondata,
              error: function (error) {
                console.log(error);
                ConnectionFailed();
              },
              success: (response) => {
                let res = JSON.parse(response);
                if (res.statusCode == 200) {
                  if (res.data.length != 0) {
                    let mobile = res.data[0].RPHONE;
                    let phoneStr = mobile.replace(/\s/g, "");
                    phoneStr = phoneStr.replace(/[^A-Z0-9]/gi, "");
                    mobilephone = phoneStr.substring(0, 10);
                    wherelikeMobile =
                      "%" +
                      mobilephone.substring(0, 3) +
                      "%" +
                      mobilephone.substring(3, 6) +
                      "%" +
                      mobilephone.substring(6, 11) +
                      "%";
                    patientAccept();
                  } else {
                    Swal.fire({
                      title: "ไม่พบข้อมูลผู้ป่วย",
                      text: "เลขประจำตัวผู้ป่วย หรือ เลขประจำตัวประชาชน หรือ วัน/เดือน/ปี เกิด หรือ หมายเลขโทรศัพท์มือถือ 'ไม่ถูกต้อง' กรุณาตรวสอบความถูกต้องของข้อมูลข้างต้นแล้วลองใหม่อีกครั้ง หรือติดต่อเวชระเบียน \nหรือ โทร. 074-451051-2 \nหรือ ติดต่อผ่านทาง Line @psu2525 เพื่อตรวจสอบข้อมูล",
                      icon: "warning",
                      showCancelButton: false,
                      confirmButtonColor: "#3085d6",
                      confirmButtonText: "ตกลง",
                    }).then(() => {
                      // $("#verifyModal").modal("show");
                      window.location.reload();
                    });
                  }
                } else {
                  console.log("Connection Error");
                  ConnectionFailed();
                  // Swal.fire({
                  //   timer: 3000,
                  //   icon: "warning",
                  //   title: "ไม่สามารถเชื่อมต่อเครือข่ายได้",
                  //   text: "กรุณาตรวจสอบการเชื่อมต่ออินเทอร์เน็ตแล้วลองใหม่อีกครั้ง",
                  //   showConfirmButton: true,
                  //   // footer: '',
                  //   confirmButtonText: "ตกลง",
                  // }).then(() => {
                  //   closeWindow();
                  // });
                }
              },
            });
          } else {
            Swal.fire({
              title: "หมายเลขผู้ป่วย(HN) หรือ หมายเลขบัตรประชาชน ไม่ถูกต้อง",
              text: "กรุณาตรวจสอบข้อมูลแล้วลองใหม่อีกครั้ง",
              icon: "error",
              showCancelButton: false,
              confirmButtonColor: "#3085d6",
              confirmButtonText: "ตกลง",
            }).then(() => {
              $("#verifyModal").modal("show");
            });
          }
        } else {
          Swal.fire({
            title:
              "หมายเลขผู้ป่วย(HN) หรือ หมายเลขบัตรประชาชน หรือ วัน/เดือน/ปี เกิด หรือ หมายเลขโทรศัพท์มือไม่ถูกต้อง",
            text: "กรุณาตรวจสอบข้อมูลแล้วลองใหม่อีกครั้ง",
            icon: "error",
            showCancelButton: false,
            confirmButtonColor: "#3085d6",
            confirmButtonText: "ตกลง",
          }).then(() => {
            $("#verifyModal").modal("show");
          });
        }
      } else {
        console.log("Recaptcha not success!");
      }
    },
  });
}

function moreDetail() {
  Swal.fire({
    title: "รายละเอียดเพิ่มเติม",
    text: "หากประสงค์รับยากลับบ้าน ระบบจะแสดงข้อมูลแก่แพทย์เพื่อพิจารณาความเหมาะสมอีกครั้ง หากดำเนินการสำเร็จ ท่านจะได้รับยา พร้อมบัตรนัดใหม่ทางไปรษณีย์",
    footer: "หากมีข้อสงสัยเพิ่มเติมกรุณาติดต่อคลินิกเดิมที่ท่านรับบริการ",
    icon: "warning",
    showCancelButton: false,
    confirmButtonColor: "#3085d6",
    confirmButtonText: "ตกลง",
    allowOutsideClick: false,
  }).then(() => {
    patientAccept();
  });
}

function patientAccept() {
  let body = {
    action: "patientaccept",
    data: {
      hn: hn,
    },
  };
  $.ajax({
    url: "libs/ApiService.php", //the page containing php script
    headers: {
      "Content-Type": "application/json; charset=utf-8",
    },
    type: "POST", //request type,
    data: JSON.stringify(body),
    // data: jsondata,
    error: function (error) {
      console.log(error);
      ConnectionFailed();
    },
    success: (response) => {
      let res = JSON.parse(response);
      if (res.statusCode == 200) {
        // console.log(res);
        if (res.data.length != 0) {
          let dateAccepted;
          allClinic = res.data;
          if (
            res.data[0].DATE_ACCEPT == "0000-00-00" ||
            res.data[0].DATE_ACCEPT == null
          ) {
            dateAccepted = new Date();
          } else {
            dateAccepted = new Date(res.data[0].DATE_ACCEPT);
          }
          let curdate = new Date();

          let differenceTime = curdate - dateAccepted;
          let differenceDays = Math.round(
            Math.abs(differenceTime / (24 * 60 * 60 * 1000))
          );
          // !Change data within 15 days
          // console.log(res.data[0].DATE_ACCEPT);
          console.log("Change data in: " + differenceDays + " day.");
          if (differenceDays <= 15) {
            Swal.fire({
              title: "ผู้รับบริการยินยอมที่จะรับบริการส่งยากลับบ้านหรือไม่",
              // text: `บริการส่งยากลับบ้าน มีค่าใช้จ่าย 'เฉพาะค่าจัดส่ง' ที่เรียกเก็บเงินปลายทาง ราคา 50-300 บาท \n
              //   (ตามขนาดกล่อง/น้ำหนักพัสดุ) \n`,
              html: `
                  <p>บริการส่งยากลับบ้าน มีค่าใช้จ่าย 'เฉพาะค่าจัดส่ง' ที่เรียกเก็บเงินปลายทาง ราคา 50-300 บาท <br /> (ตามขนาดกล่อง/น้ำหนักพัสดุ) <br />
                  <span class="mt-2 mb-2 badge badge-primary p-2" style="font-size:18px; line-height: 1.8;">ท่านจะได้รับยาภายใน 7 วัน <br/>ก่อนถึงวันนัด</span> <br /> 
                  ท่านยินยอมรับบริการหรือไม่</p>
                  <a class="text-info" onclick="moreDetail()" style="cursor: pointer;">รายละเอียดเพิ่มเติม</a>`,
              footer:
                "หากว่ามีข้อสงสัย กรุณาโทรสอบถามตามหมายเลขโทรศัพท์ในใบนัดเดิม",
              icon: "warning",
              showCancelButton: true,
              confirmButtonColor: "#3085d6",
              confirmButtonText: "ยินยอม",
              cancelButtonColor: "#d33",
              cancelButtonText: "ไม่ยินยอม",
            }).then((result) => {
              if (result.isConfirmed) {
                // console.log(allClinic);
                $("#verifyModal").modal("hide");
                // checkPayment();
                patientAddressInfo();
                if (mobilephone === "") {
                  let homeappointphone = res.data[0].PHONE;
                  let phoneStr = homeappointphone.replace(/\s/g, "");
                  phoneStr = phoneStr.replace(/[^A-Z0-9]/gi, "");
                  mobilephone = phoneStr.substring(0, 10);
                  wherelikeMobile =
                    "%" +
                    mobilephone.substring(0, 3) +
                    "%" +
                    mobilephone.substring(3, 6) +
                    "%" +
                    mobilephone.substring(6, 11) +
                    "%";
                }
                $("#fullname").text(res.data[0].PNAME);
                $("#appoint").text(
                  "คุณมีนัดทั้งหมด: " + res.data.length + " นัด"
                );

                res.data.forEach((element, index) => {
                  let check = "checkappoint" + (index + 1);
                  let dateapTH =
                    convertToDateTH(element.DATE_AP) +
                    " " +
                    element.TIME_AP +
                    " น.";
                  let doctname;
                  if (element.DOCT_REG != "") {
                    doctname = element.DOCT_REG;
                  } else {
                    doctname = "-";
                  }
                  let cliniccard = `<div class="col">
                                        <input class="form-check-input" type="checkbox" value="${index}" id="${check}">
                                        <label class="form-check-label" for="flexCheckDefault">
                                          <div class="mb-3 col-auto text-center clinic-card border border-secondary rounded">
                                            <div class="card-body">
                                              <div class="row">
                                                <div class="col-auto">
                                                  <p style="margin-bottom:0; font-size: 18px; font-weight:bold;"><i class="fas fa-notes-medical"></i> คลินิก: </p>
                                                </div>
                                                <div class="col-auto">
                                                  <p class="text-black" style="margin-bottom:0; font-size: 18px;">${element.UNIT_AP}</p>
                                                </div>
                                              </div>
                                              <div class="row">
                                                <div class="col-auto">
                                                  <p style="margin-bottom:0; font-size: 18px; font-weight:bold;"><i class="fas fa-calendar-day"></i> วันที่นัด: </p>
                                                </div>
                                                <div class="col-auto">
                                                  <p class="text-black" style="margin-bottom:0; font-size: 18px;">${dateapTH}</p>
                                                </div>
                                              </div>
                                              <div class="row">
                                                <div div class="col-auto">
                                                  <p style="margin-bottom:0; font-size: 18px; font-weight:bold;"><i class="fas fa-user-md"></i> แพทย์: </p>
                                                </div>
                                                <div class="col-auto">
                                                  <p class="text-black" style="margin-bottom:0; font-size: 18px;">${doctname}</p>
                                                </div>
                                              </div>
                                            </div>
                                            </div>
                                          </div>
                                        </label>
                                      </div>`;
                  $("#checkboxlist").append(cliniccard);
                });

                if (
                  res.data[0].DATE_ACCEPT != null &&
                  res.data[0].DATE_ACCEPT != "0000-00-00" &&
                  (res.data[0].PATIENT_ACCEPT != "" ||
                    res.data[0].PATIENT_ACCEPT != null)
                ) {
                  // $("#inputRecipientName").attr("disabled", "disabled");
                  // $("#inputAddress").attr("disabled", "disabled");
                  // $("#inputAddress2").attr("disabled", "disabled");
                  // $("#inputAddress3").attr("disabled", "disabled");
                  // $("#selectChangWat").attr("disabled", "disabled");
                  // $("#selectAmphur").attr("disabled", "disabled");
                  // $("#selectTumbon").attr("disabled", "disabled");
                  // $("#inputPost").attr("disabled", "disabled");
                  // $("#inputMobilePhone").attr("disabled", "disabled");
                  // $("#confirmButton").attr("hidden", true);

                  $("#confirmaddress-tab").removeClass("active");
                  $("#confirmaddress-tab").attr("aria-selected", "false");
                  $("#confirmaddress").removeClass("active show");

                  $("#payment-tab").addClass("nav-link active");
                  $("#payment-tab").attr("aria-selected", "true");
                  $("#payment").addClass("active show");
                  checkPayment();
                } else {
                  $("#confirmButton").attr("hidden", false);
                }
              } else if (result.dismiss === Swal.DismissReason.cancel) {
                let patientappointUpdate = [];
                allClinic.forEach((element) => {
                  patientappointUpdate = [];
                  patientappointUpdate = {
                    dateap: element.DATE_AP,
                    codeap: element.CODE_AP,
                    cdoct: element.C_DOCT,
                    hn: element.HN,
                    accept: "N",
                  };
                  UpdatePatientAccepted(patientappointUpdate);
                });
              }
            });
          } else {
            Swal.fire({
              title: "หมดเวลาแก้ไขข้อมูลที่อยู่\nเพื่อรับยาที่บ้าน",
              // text: "คุณได้ยืนยันข้อมูลที่อยู่และ 'ยินยอม' ที่จะรับยาที่บ้าน",
              icon: "warning",
              showCancelButton: false,
              confirmButtonColor: "#3085d6",
              confirmButtonText: "ตกลง",
            });
            // .then(() => {
            //   closeWindow();
            //   // window.location.replace("https://hospital.psu.ac.th/");
            // });
          }
        } else {
          // console.log("Find in mydata appoint");
          getMyDataAppoint();
          // Swal.fire({
          //   title: "ไม่พบข้อมูลนัดผู้ป่วย",
          //   text: "กรุณาติดต่อเวชระเบียน \nหรือ โทร. 074-451051-2 \nหรือ ติดต่อผ่านทาง Line @psu2525 เพื่อตรวจสอบข้อมูล",
          //   icon: "warning",
          //   showCancelButton: false,
          //   confirmButtonColor: "#3085d6",
          //   confirmButtonText: "ตกลง",
          // }).then(() => {
          //   $("#verifyModal").modal("show");
          // });
        }
      } else {
        Swal.fire({
          timer: 3000,
          icon: "warning",
          title: "ไม่สามารถเชื่อมต่อเครือข่ายได้",
          text: "กรุณาตรวจสอบการเชื่อมต่ออินเทอร์เน็ตแล้วลองใหม่อีกครั้ง",
          showConfirmButton: true,
          // footer: '',
          confirmButtonText: "ตกลง",
        }).then(() => {
          closeWindow();
        });
      }
    },
  });
}

// Select APpoint from Mydata.Appoint
function getMyDataAppoint() {
  let body = {
    action: "mydataappoint",
    data: {
      hn: hn,
    },
  };
  $.ajax({
    url: "libs/ApiService.php", //the page containing php script
    headers: {
      "Content-Type": "application/json; charset=utf-8",
    },
    type: "POST", //request type,
    data: JSON.stringify(body),
    // data: jsondata,
    error: function (error) {
      console.log(error);
      ConnectionFailed();
    },
    success: (response) => {
      let res = JSON.parse(response);
      if (res.statusCode === 200) {
        if (res.data.length != 0) {
          res.data.forEach((element) => {
            updateHomeAppoint(element);
          });
        } else {
          Swal.fire({
            title: "ไม่พบข้อมูลนัดผู้ป่วย",
            text: "กรุณาติดต่อเวชระเบียน \nหรือ โทร. 074-451051-2 \nหรือ ติดต่อผ่านทาง Line @psu2525 เพื่อตรวจสอบข้อมูล",
            icon: "warning",
            showCancelButton: false,
            confirmButtonColor: "#3085d6",
            confirmButtonText: "ตกลง",
          }).then(() => {
            $("#verifyModal").modal("show");
          });
        }
      } else {
        Swal.fire({
          timer: 3000,
          icon: "warning",
          title: "ไม่สามารถเชื่อมต่อเครือข่ายได้",
          text: "กรุณาตรวจสอบการเชื่อมต่ออินเทอร์เน็ตแล้วลองใหม่อีกครั้ง",
          showConfirmButton: true,
          // footer: '',
          confirmButtonText: "ตกลง",
        }).then(() => {
          closeWindow();
        });
      }
    },
  });
}

function updateHomeAppoint(body) {
  // console.log(body);
  // console.log(mobilephone);
  let appointbody = {
    action: "insertmydataappoint",
    data: {
      hn: body.HN,
      pname: body.PNAME,
      dateap: body.DATE_AP,
      timeap: body.TIME_AP,
      unitap: body.UNIT_AP,
      codeap: body.CODE_AP,
      datereg: body.DATE_REG,
      wardreg: body.WARD_REG,
      doctreg: body.DOCT_REG,
      cdoct: body.C_DOCT,
      prnst: body.PRN_ST,
      frdoct: body.FR_DOCT,
      comment: body.COMMENT,
      phone: mobilephone,
    },
  };
  $.ajax({
    url: "libs/ApiService.php", //the page containing php script
    headers: {
      "Content-Type": "application/json; charset=utf-8",
    },
    type: "POST", //request type,
    data: JSON.stringify(appointbody),
    // data: jsondata,
    error: function (error) {
      console.log(error);
      ConnectionFailed();
    },
    success: (response) => {
      if (response.statusCode === 200) {
        patientAccept();
      }
    },
  });
}

function patientAddressInfo() {
  let body = {
    action: "patientaddress",
    data: {
      hn: hn,
      phone: wherelikeMobile,
    },
  };
  $.ajax({
    url: "libs/ApiService.php", //the page containing php script
    headers: {
      "Content-Type": "application/json; charset=utf-8",
    },
    type: "POST", //request type,
    data: JSON.stringify(body),
    // data: jsondata,
    error: function (error) {
      console.log(error);
      ConnectionFailed();
    },
    success: (response) => {
      let res = JSON.parse(response);
      if (res.statusCode == 200) {
        if (res.data.length != 0) {
          confirmAction = "UPDATE";
          let patientappointdata = res.data[0];
          let recipientname = patientappointdata.names;
          let address = patientappointdata.address;
          let address2 = patientappointdata.address2;
          let address3 = patientappointdata.address3;
          // let phone = patientappointdata.phone;
          let changwat = patientappointdata.changwat;
          let amphur = patientappointdata.amphur;
          let tumbon = patientappointdata.tumbon;
          $("#inputRecipientName").val(recipientname);
          $("#inputAddress").val(address);
          $("#inputAddress2").val(address2);
          $("#inputAddress3").val(address3);
          $("#inputMobilePhone").val(mobilephone);
          getAllChangWat(changwat);
          getAllAmphur(changwat, amphur);
          getAllTumbon(changwat, amphur, tumbon);
        } else {
          confirmAction = "INSERT";
          patientAddressMedrec(body);
        }
      } else {
        Swal.fire({
          timer: 3000,
          icon: "warning",
          title: "ไม่สามารถเชื่อมต่อเครือข่ายได้",
          text: "กรุณาตรวจสอบการเชื่อมต่ออินเทอร์เน็ตแล้วลองใหม่อีกครั้ง",
          showConfirmButton: true,
          // footer: '',
          confirmButtonText: "ตกลง",
        }).then(() => {
          closeWindow();
        });
      }
    },
  });
}

function patientAddressMedrec(body) {
  body.action = "patientaddressmedrec";
  // console.log(body);
  $.ajax({
    url: "libs/ApiService.php", //the page containing php script
    headers: {
      "Content-Type": "application/json; charset=utf-8",
    },
    type: "POST", //request type,
    data: JSON.stringify(body),
    // data: jsondata,
    error: function (error) {
      console.log(error);
      ConnectionFailed();
    },
    success: (response) => {
      let res = JSON.parse(response);
      if (res.statusCode == 200) {
        if (res.data.length != 0) {
          let patientappointdata = res.data[0];
          let recipientname =
            patientappointdata.PINITIAL +
            patientappointdata.PNAME +
            " " +
            patientappointdata.PSUR;
          let address = patientappointdata.PADDRESS;
          // let phone = patientappointdata.phone;
          let area = patientappointdata.PAREA;
          let changwat = area.substring(0, 2);
          let amphur = area.substring(2, 4);
          let tumbon = area.substring(4, 6);

          $("#inputRecipientName").val(recipientname);
          $("#inputAddress").val(address);
          $("#inputMobilePhone").val(mobilephone);
          // $("#inputAddress2").val(address2);
          // $("#inputAddress3").val(address3);

          getAllChangWat(changwat);
          getAllAmphur(changwat, amphur);
          getAllTumbon(changwat, amphur, tumbon);
        } else {
          Swal.fire({
            title: "ไม่พบข้อมูลผู้ป่วย",
            text: "กรุณาติดต่อเวชระเบียน \nหรือ โทร. 074-451051-2 \nหรือ ติดต่อผ่านทาง Line @psu2525 เพื่อตรวจสอบข้อมูล",
            icon: "warning",
            showCancelButton: false,
            confirmButtonColor: "#3085d6",
            confirmButtonText: "ตกลง",
          }).then(() => {
            $("#verifyModal").modal("show");
          });
        }
      } else {
        Swal.fire({
          timer: 3000,
          icon: "warning",
          title: "ไม่สามารถเชื่อมต่อเครือข่ายได้",
          text: "กรุณาตรวจสอบการเชื่อมต่ออินเทอร์เน็ตแล้วลองใหม่อีกครั้ง",
          showConfirmButton: true,
          // footer: '',
          confirmButtonText: "ตกลง",
        }).then(() => {
          closeWindow();
        });
      }
    },
  });
}

function getAllChangWat(cwat) {
  let body = {
    action: "allchangwat",
  };
  $.ajax({
    url: "libs/ApiService.php", //the page containing php script
    headers: {
      "Content-Type": "application/json; charset=utf-8",
    },
    type: "POST", //request type,
    data: JSON.stringify(body),
    // data: jsondata,
    error: function (error) {
      console.log(error);
      ConnectionFailed();
    },
    success: (response) => {
      let res = JSON.parse(response);
      // console.log(res);
      if (res.statusCode == 200) {
        let optioneElement;
        let optionValue;
        let optionText;
        $("#selectChangWat").empty();
        res.data.forEach((element) => {
          optionValue = element.CWAT;
          optionText = element.NWAT;
          optioneElement = `<option value="${optionValue}">${optionText}</option>`;
          $("#selectChangWat").append(optioneElement);
        });
        $("#selectChangWat option[value=" + cwat + "]").attr("selected", true);
      } else {
        Swal.fire({
          timer: 3000,
          icon: "warning",
          title: "ไม่สามารถเชื่อมต่อเครือข่ายได้",
          text: "กรุณาตรวจสอบการเชื่อมต่ออินเทอร์เน็ตแล้วลองใหม่อีกครั้ง",
          showConfirmButton: true,
          // footer: '',
          confirmButtonText: "ตกลง",
        }).then(() => {
          closeWindow();
        });
      }
    },
  });
}

function getAllAmphur(cwat, cphur) {
  let body = {
    action: "allamphur",
    data: {
      cwat: cwat,
    },
  };
  $.ajax({
    url: "libs/ApiService.php", //the page containing php script
    headers: {
      "Content-Type": "application/json; charset=utf-8",
    },
    type: "POST", //request type,
    data: JSON.stringify(body),
    // data: jsondata,
    error: function (error) {
      console.log(error);
      ConnectionFailed();
    },
    success: (response) => {
      let res = JSON.parse(response);
      // console.log(res);
      if (res.statusCode == 200) {
        let optioneElement;
        let optionValue;
        let optionText;
        $("#selectAmphur").empty();
        res.data.forEach((element) => {
          optionValue = element.CPHUR;
          optionText = element.NPHUR;
          optioneElement = `<option value="${optionValue}">${optionText}</option>`;
          $("#selectAmphur").append(optioneElement);
        });
        $("#selectAmphur option[value=" + cphur + "]").attr("selected", true);
      } else {
        Swal.fire({
          timer: 3000,
          icon: "warning",
          title: "ไม่สามารถเชื่อมต่อเครือข่ายได้",
          text: "กรุณาตรวจสอบการเชื่อมต่ออินเทอร์เน็ตแล้วลองใหม่อีกครั้ง",
          showConfirmButton: true,
          // footer: '',
          confirmButtonText: "ตกลง",
        }).then(() => {
          closeWindow();
        });
      }
    },
  });
}

function getAllTumbon(cwat, cphur, cbon) {
  let body = {
    action: "alltumbon",
    data: {
      cwatphur: cwat + cphur,
    },
  };
  $.ajax({
    url: "libs/ApiService.php", //the page containing php script
    headers: {
      "Content-Type": "application/json; charset=utf-8",
    },
    type: "POST", //request type,
    data: JSON.stringify(body),
    // data: jsondata,
    error: function (error) {
      console.log(error);
      ConnectionFailed();
    },
    success: (response) => {
      let res = JSON.parse(response);
      // console.log(res);
      if (res.statusCode == 200) {
        let optioneElement;
        let optionValue;
        let optionText;
        let optionPost;
        $("#selectTumbon").empty();
        res.data.forEach((element) => {
          // console.log(element);
          optionValue = element.CBON;
          optionText = element.NBON;
          optionPost = element.CPOST;
          optioneElement = `<option value="${optionValue}" postal="${optionPost}">${optionText}</option>`;
          $("#selectTumbon").append(optioneElement);
        });
        $("#selectTumbon option[value=" + cbon + "]").attr("selected", true);
        // console.log($("#selectTumbon option:selected").attr("post"));
        $("#inputPost").val($("#selectTumbon option:selected").attr("postal"));
      } else {
        Swal.fire({
          timer: 3000,
          icon: "warning",
          title: "ไม่สามารถเชื่อมต่อเครือข่ายได้",
          text: "กรุณาตรวจสอบการเชื่อมต่ออินเทอร์เน็ตแล้วลองใหม่อีกครั้ง",
          showConfirmButton: true,
          // footer: '',
          confirmButtonText: "ตกลง",
        }).then(() => {
          closeWindow();
        });
      }
    },
  });
}

function recheckAddress() {
  // console.log(allClinic);
  // console.log(ckeckedList);
  ckeckedList = [];
  allClinic.forEach((element, index) => {
    let checkedClinic = $("#checkappoint" + (index + 1) + ":checked").val();
    if (checkedClinic != undefined) {
      // console.log(allClinic[checkedClinic]);
      ckeckedList.push(allClinic[checkedClinic]);
    }
  });

  if (ckeckedList.length != 0) {
    let recipientName = $("#inputRecipientName").val();
    let address = $("#inputAddress").val();
    let address2 = $("#inputAddress2").val();
    let address3 = $("#inputAddress3").val();
    let changwat = $("#selectChangWat option:selected").val();
    let amphur = $("#selectAmphur option:selected").val();
    let tumbon = $("#selectTumbon option:selected").val();
    let postal = $("#selectTumbon option:selected").attr("postal");
    let mobile = $("#inputMobilePhone").val();
    if (recipientName != "" && address != "") {
      let fulladdress =
        address +
        " " +
        address2 +
        " " +
        address3 +
        " " +
        $("#selectTumbon option:selected").text() +
        " " +
        $("#selectAmphur option:selected").text() +
        " " +
        "จ." +
        $("#selectChangWat option:selected").text() +
        " " +
        postal;
      $("#receiver").text(recipientName);
      $("#address").text(fulladdress);
      $("#mobile").text(mobile);
      $("#cliniccount").text(ckeckedList.length + " คลินิก");
      $("#detailAddressModal").modal("show");
      confirmaddressbody = {
        action: "addressconfirm",
        method: confirmAction,
        data: {
          hn: hn,
          phone: mobilephone,
          names: recipientName,
          address: address,
          address2: address2,
          address3: address3,
          changwat: changwat,
          amphur: amphur,
          tumbon: tumbon,
          postal: postal,
        },
      };
    } else {
      Swal.fire({
        icon: "warning",
        title: "กรุณากรอกข้อมูลผู้รับ และ ที่อยู่ให้ครบถ้วน",
        showConfirmButton: true,
        // footer: '',
        confirmButtonText: "ตกลง",
      });
    }
  } else {
    Swal.fire({
      timer: 3000,
      icon: "warning",
      title: "ยังไม่เลือกคลินิกที่ต้องการรับยาที่บ้าน",
      text: "กรุณาเลือกคลินิกที่ต้องการรับยาที่บ้านก่อนแล้วยืนยันข้อมูลอีกครั้ง",
      showConfirmButton: true,
      // footer: '',
      confirmButtonText: "ตกลง",
    });
  }
}

function confirmAddress() {
  // console.log(confirmaddressbody);
  $.ajax({
    // url:"https://his01.psu.ac.th/HosApp/trackingdrugs/libs/SendMail.php", //the page containing php script
    url: "libs/ApiService.php", //the page containing php script
    headers: {
      "Content-Type": "application/json; charset=utf-8",
    },
    type: "POST", //request type,
    data: JSON.stringify(confirmaddressbody),
    // data: jsondata,
    error: function (error) {
      console.log(error);
      ConnectionFailed();
    },
    success: () => {
      let patientappointUpdate = [];
      ckeckedList.forEach((element) => {
        patientappointUpdate = [];
        patientappointUpdate = {
          dateap: element.DATE_AP,
          codeap: element.CODE_AP,
          cdoct: element.C_DOCT,
          hn: element.HN,
          accept: "Y",
        };
        UpdatePatientAccepted(patientappointUpdate);
      });
    },
  });
}

function UpdatePatientAccepted(patientappointdata) {
  // console.log(patientappointdata);
  let body = {
    action: "updatepatientaccepted",
    data: patientappointdata,
  };
  // console.log(body);
  $.ajax({
    url: "libs/ApiService.php", //the page containing php script
    headers: {
      "Content-Type": "application/json; charset=utf-8",
    },
    type: "POST", //request type,
    data: JSON.stringify(body),
    // data: jsondata,
    error: function (error) {
      console.log(error);
      ConnectionFailed();
    },
    success: () => {
      // let res = JSON.parse(response);
      // console.log(response);
      // if (res.statusCode === 200) {
      let accept = patientappointdata.accept;
      switch (accept) {
        case "Y":
          Swal.fire({
            timer: 3000,
            icon: "success",
            title: "คุณยืนยันที่จะรับบริการส่งยากลับบ้าน",
            showConfirmButton: false,
          }).then(() => {
            // window.location.reload();
            // closeWindow();
            $("#detailAddressModal").modal("hide");
            $("#inputRecipientName").attr("disabled", "disabled");
            $("#inputAddress").attr("disabled", "disabled");
            $("#inputAddress2").attr("disabled", "disabled");
            $("#inputAddress3").attr("disabled", "disabled");
            $("#selectChangWat").attr("disabled", "disabled");
            $("#selectAmphur").attr("disabled", "disabled");
            $("#selectTumbon").attr("disabled", "disabled");
            $("#inputPost").attr("disabled", "disabled");
            $("#inputMobilePhone").attr("disabled", "disabled");
            $("#confirmButton").attr("hidden", true);

            $("#confirmaddress-tab").removeClass("active");
            $("#confirmaddress-tab").attr("aria-selected", "false");
            $("#confirmaddress").removeClass("active show");

            $("#payment-tab").addClass("nav-link active");
            $("#payment-tab").attr("aria-selected", "true");
            $("#payment").addClass("active show");
            checkPayment();
          });
          break;
        case "N":
          Swal.fire({
            timer: 3000,
            icon: "success",
            title: "คุณยืนยันที่จะไม่รับบริการส่งยากลับบ้าน",
            showConfirmButton: false,
          }).then(() => {
            // window.location.reload();
            closeWindow();
          });
          break;
        default:
          break;
      }
      // }
    },
  });
}

function trackingNumber() {
  //   console.log(allClinic);
  if (allClinic.length != 0) {
    let jsondata = [];
    allClinic.forEach((element, index) => {
      // trackingdrugs
      let body = {
        action: "trackingdrugs",
        data: {
          hn: element.HN,
          dateap: element.DATE_AP,
          cdoct: element.C_DOCT,
          codeap: element.CODE_AP,
        },
      };
      $.ajax({
        url: "libs/ApiService.php", //the page containing php script
        headers: {
          "Content-Type": "application/json; charset=utf-8",
        },
        type: "POST", //request type,
        data: JSON.stringify(body),
        // data: jsondata,
        error: function (error) {
          console.log(error);
          ConnectionFailed();
          $("#qrcodeModal").modal("hide");
        },
        success: (response) => {
          let res = JSON.parse(response);
          if (res.statusCode === 200) {
            if (res.data.length != 0) {
              $("#trackingempty").attr("hidden", true);
              $("#trackingsuccess").attr("hidden", false);
              res.data.forEach((element) => {
                jsondata.push({
                  scgexpress: `<button
                  class="btn btn-success"
                  onclick="SCGExpress(${element.tracking_no})"
                >
                  <i class="fas fa-truck"></i> ${element.tracking_no}
                </button>`,
                  senddate: element.senddate,
                  orderid: element.orderid,
                });
                // $("#trackingModal").modal("show");
              });
              // console.log("===============================");
              // console.log(jsondata);

              trackingtable = $("#trackingtable").DataTable({
                // "scrollX": true,
                searching: false,
                responsive: true,
                bPaginate: false,
                ordering: false,
                destroy: true,
                info: false,
                order: [[0, "desc"]],
                bLengthChange: false,
                aLengthMenu: [5, 15, 25],
                iDisplayLength: 5,
                data: jsondata,

                columns: [
                  { data: "scgexpress" },
                  { data: "senddate" },
                  { data: "orderid" },
                ],
                // columnDefs: [
                //   {
                //     targets: 7, // this means controlling cells in column 1
                //     render: function (data, type, row, meta) {
                //       let btnTracking;
                //       btnTracking = `<button
                //         class="btn btn-success"
                //         onclick="SCGExpress()"
                //       >
                //         <i class="fas fa-truck"></i> ติดตามพัสดุ
                //       </button>`;

                //       return btnTracking;
                //     },
                //   },
                //   //   {
                //   //     targets: 6, // this means controlling cells in column 1
                //   //     render: function (data, type, row, meta) {
                //   //       let detailBtn = "<button id='detailbtn' class='btn btn-info' onclick='onDetailModal()'><i class='fas fa-file-alt' style='font-size: 16px;'></i></button >";
                //   //       return detailBtn;
                //   //     }
                //   //   }
                // ],
              });
            } else {
              $("#trackingempty").attr("hidden", false);
              $("#trackingsuccess").attr("hidden", true);
              console.log("Tracking is empty!");
            }
          } else {
            console.log("Response Error");
          }
        },
      });
    });
  } else {
    console.log("Empty clinic data");
  }
}

function closeWindow() {
  // window.location.replace("https://hospital.psu.ac.th/");
  window.location.reload();
}

function ConnectionFailed() {
  Swal.fire({
    title: "ไม่สามารถเชื่อมต่อเครือข่ายได้",
    text: "",
    icon: "warning",
    showCancelButton: false,
    confirmButtonColor: "#3085d6",
    confirmButtonText: "ตกลง",
  }).then(() => {
    $("#verifyModal").modal("show");
  });
}

function SCGExpress(tracking) {
  let trackingBase64Encode = btoa(tracking);
  //   console.log(trackingBase64Encode);
  window.open(
    "https://www.scgexpress.co.th/tracking/detail/" + trackingBase64Encode
  );
}

function convertToDateTH(date) {
  // console.log(date);
  let dateNow = new Date(date.replace(" ", "T"));
  let dateTH = dateNow.getDate();
  let monthTH = dateNow.getMonth() + 1;
  let yearTH = dateNow.getFullYear() + 543;
  let dateTHStr;
  let monthTHStr;
  let fulldateTH;
  if (parseInt(dateTH) < 9) {
    dateTHStr = "0" + dateTH;
  } else {
    dateTHStr = dateTH;
  }

  switch (monthTH) {
    case 1:
      monthTHStr = "ม.ค.";
      break;
    case 2:
      monthTHStr = "ก.พ.";
      break;
    case 3:
      monthTHStr = "มี.ค.";
      break;
    case 4:
      monthTHStr = "เม.ย.";
      break;
    case 5:
      monthTHStr = "พ.ค.";
      break;
    case 6:
      monthTHStr = "มิ.ย.";
      break;
    case 7:
      monthTHStr = "ก.ค.";
      break;
    case 8:
      monthTHStr = "ส.ค.";
      break;
    case 9:
      monthTHStr = "ก.ย.";
      break;
    case 10:
      monthTHStr = "ต.ค.";
      break;
    case 11:
      monthTHStr = "พ.ย.";
      break;
    case 12:
      monthTHStr = "ธ.ค.";
      break;
    default:
      break;
  }
  fulldateTH =
    dateTHStr + " " + monthTHStr + " " + yearTH.toString().substring(2, 5);
  return fulldateTH;
}

function hideConfirmAddress() {
  $("#confirmaddress-tab").removeClass("active");
  $("#confirmaddress-tab").attr("aria-selected", "false");
  $("#confirmaddress").removeClass("active show");
}

function showConfirmAddress() {
  $("#confirmaddress-tab").addClass("nav-link active");
  $("#confirmaddress-tab").attr("aria-selected", "true");
  $("#confirmaddress").addClass("active show");
}

function hidePayment() {
  $("#payment-tab").removeClass("active");
  $("#payment-tab").attr("aria-selected", "false");
  $("#payment").removeClass("active show");
}

function showPayment() {
  $("#payment-tab").addClass("nav-link active");
  $("#payment-tab").attr("aria-selected", "true");
  $("#payment").addClass("active show");
}

function hideTracking() {
  $("#tracking-tab").removeClass("active");
  $("#tracking-tab").attr("aria-selected", "false");
  $("#tracking").removeClass("active show");
}

function showTracking() {
  $("#tracking-tab").addClass("nav-link active");
  $("#tracking-tab").attr("aria-selected", "true");
  $("#tracking").addClass("active show");
}

function bankTransfer() {
  $("#banktransfer").modal("show");
}
