<?php
header('Content-Type: text/html; charset=utf-8');
$postdata = file_get_contents("php://input");
$path = __DIR__ . "/../logs/logs-service-" . date("Y-m-d") . ".txt";
if ($postdata != "") {
    # code...
    $request = json_decode($postdata);
    switch ($request->action) {
        case 'verifypatient':
            $verify = new ApiService();
            $verify->VerifyPatient($request->data);
            break;

        case 'patientaccept':
            $patientaccept = new ApiService();
            $patientaccept->PatientAccept($request->data);
            break;

        case 'mydataappoint':
            $mydataappoint = new ApiService();
            $mydataappoint->MyDataAppoint($request->data);
            break;

        case 'insertmydataappoint':
            $insertmydataappoint = new ApiService();
            $insertmydataappoint->InsertMyDataAppoint($request->data);
            break;

        case 'checkpaymenthistory':
            $paymenthistory = new ApiService();
            $paymenthistory->CheckPaymentHistory($request->data);
            break;

        case "patientaddress":
            $address = new ApiService();
            $address->PatientAddress($request->data);
            break;

        case "patientaddressmedrec":
            $addressmedrec = new ApiService();
            $addressmedrec->PatientAddressMedrec($request->data);
            break;

        case "allchangwat":
            $changwat = new ApiService();
            $changwat->AllChangWat();
            break;

        case "allamphur":
            $amphur = new ApiService();
            $amphur->AllAmphur($request->data);
            break;

        case "alltumbon":
            $tumbon = new ApiService();
            $tumbon->AllTumbon($request->data);
            break;

        case "addressconfirm":
            $confirm = new ApiService();
            if ($request->method == "INSERT") {
                $confirm->InsertAddressConfirm($request->data);
            } else if ($request->method == "UPDATE") {
                $confirm->UpdateAddressConfirm($request->data);
            }
            break;

        case "updatepatientaccepted":
            $accepted = new ApiService();
            $accepted->UpdatePatientAccepted($request->data);
            $logjsondata = array(
                "date" => date("Y-m-d h:i:s"),
                "hn" => $hn,
                "invoiceid" => $invoiceid,
                "ref2" => $ref2,
                "sql" => $sql,
            );
            $newRequestData = date("Y-m-d H:i:s") . "\tdata: " . trim(preg_replace('/\s/', '', json_encode($request->data))) . PHP_EOL;
            @file_put_contents($path, $newRequestData, FILE_APPEND);
            break;

        case 'captchaverify':
            $captcha = new ApiService();
            $captcha->CaptchaVerify($request->data);
            break;

        case 'checkallmoney':
            $allmoney = new APIService();
            $allmoney->AllMoney($request->data);
            break;

        case 'homemoney':
            $money = new ApiService();
            $money->HomeMoney($request->data);
            break;

        case 'qrcodepayment':
            $payment = new ApiService();
            $payment->QRPayment($request->data);
            $path = __DIR__ . "/../logs/logs-service-" . date("Y-m-d") . ".txt";
            $newRequestData = date("Y-m-d H:i:s") . "\tdata: " . trim(preg_replace('/\s/', '', json_encode($request->data))) . PHP_EOL;
            @file_put_contents($path, $newRequestData, FILE_APPEND);
            break;

        case 'recheckpayment':
            $recheck = new ApiService();
            $recheck->ReCheckPaymentTransfer($request->data);
            break;

        case 'updateref1ref2':
            $ref1ref2 = new ApiService();
            $ref1ref2->UpdateHomeMoneyRef1Ref2($request->data);
            break;

        case 'updatehomemoney':
            $homemoney = new ApiService();
            $homemoney->UpdateHomeMoney($request->data);
            break;

        case 'dmdatasaveonline':
            $dmonline = new ApiService();
            $dmonline->DMDataSaveOnline($request->data);
            $path = __DIR__ . "/../logs/dmsaveonline/dmdata-" . date("Y-m-d") . ".txt";
            $newRequestData = date("Y-m-d H:i:s") . "\tdata: " . trim(preg_replace('/\s/', '', json_encode($request->data))) . PHP_EOL;
            @file_put_contents($path, $newRequestData, FILE_APPEND);
            break;

        case 'opdmoneydmapprove':
            $dmapprove = new ApiService();
            $dmapprove->UpdateDMApproveID($request->data);
            break;

        case 'trackingdrugs':
            $tracking = new ApiService();
            $tracking->TrackingDrugs($request->data);
            break;

        case 'moneyscbbnconfigs':
            $scbbn = new ApiService();
            $scbbn->SCBBNLastUpdate();
            break;
        default:
            # code...
            break;
    }
}

class ApiService
{
    // *Production
    public $dsn = "mysql:host=192.168.1.13;charset=utf8;dbname=OPD";
    public $user = "AppServ";
    public $passwd = "*AppServ*";

    // *For Test
    // public $dsn = "mysql:host=172.29.1.126;charset=utf8;dbname=OPD";
    // public $user = "atiphu.s";
    // public $passwd = "#$@TIpHu.S@126$#";

    public function CaptchaVerify($data)
    {
        $url = "http://61.19.201.20:19539/medservice/googlerecaptcha";
        $ch = curl_init($url);
        $payload = json_encode($data);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);
        echo $result;
    }

    public function VerifyPatient($data)
    {
        $pdo = new PDO($this->dsn, $this->user, $this->passwd);
        $hn = $data->hn;
        $idcard = $data->idcard;
        $birthdate = $data->birthdate;
        $phone = $data->phone;
        $sql = "SELECT HN, PINITIAL, PNAME, PSUR, RPHONE, PPHONE  FROM Mydata.Medrec WHERE HN = '" . $hn . "' AND (PPHONE = '" . $idcard . "' OR DATE(BDATE) = '" . $birthdate . "' OR RPHONE LIKE '" . $phone . "') LIMIT 1 -- Select patient data by perid: 46773";
        $stm = $pdo->prepare($sql);
        $stm->execute();
        $data = $stm->fetchAll(PDO::FETCH_ASSOC);
        $allData = array(
            "statusCode" => 200,
            "data" => $data,
        );
        echo json_encode($allData);
    }

    public function PatientAccept($data)
    {
        $pdo = new PDO($this->dsn, $this->user, $this->passwd);
        $hn = $data->hn;
        $sql = "SELECT HN, PNAME, PHONE, PATIENT_ACCEPT, DATE_ACCEPT, CODE_AP, DATE_AP, TIME_AP, UNIT_AP, C_DOCT, DOCT_REG  FROM OPD.HOME_APPOINT WHERE HN = '" . $hn . "'  AND DATE_AP > DATE(CURRENT_DATE()) -- Select patient accept by perid: 46773";
        $stm = $pdo->prepare($sql);
        $stm->execute();
        $data = $stm->fetchAll(PDO::FETCH_ASSOC);
        $allData = array(
            "statusCode" => 200,
            "data" => $data,
        );
        echo json_encode($allData);
    }

    // !already use
    public function MyDataAppoint($data)
    {
        $pdo = new PDO($this->dsn, $this->user, $this->passwd);
        $hn = $data->hn;
        $sql = "SELECT * FROM Mydata.Appoint WHERE HN = '" . $hn . "'  AND DATE_AP > DATE(CURRENT_DATE()) -- Select patient accept by perid: 46773";
        // $sql = "SELECT * FROM Mydata.Appoint WHERE HN = '" . $hn . "' -- Select patient accept by perid: 46773";
        $stm = $pdo->prepare($sql);
        $stm->execute();
        $data = $stm->fetchAll(PDO::FETCH_ASSOC);
        $allData = array(
            "statusCode" => 200,
            "data" => $data,
        );
        echo json_encode($allData);
    }

    public function InsertMyDataAppoint($data)
    {
        $pdo = new PDO($this->dsn, $this->user, $this->passwd);
        $hn = $data->hn;
        $pname = $data->pname;
        $dateap = $data->dateap;
        $timeap = $data->timeap;
        $unitap = $data->unitap;
        $codeap = $data->codeap;
        $datereg = $data->datereg;
        $wardreg = $data->wardreg;
        $doctreg = $data->doctreg;
        $cdoct = $data->cdoct;
        $prnst = $data->prnst;
        $frdoct = $data->frdoct;
        $comment = $data->comment;
        $phone = $data->phone;
        // $sql = "SELECT HN, PNAME, CODE_AP, DATE_AP, TIME_AP, UNIT_AP, C_DOCT, DOCT_REG  FROM Mydata.Appoint WHERE HN = '" . $hn . "'  AND DATE_AP > DATE(CURRENT_DATE()) -- Select patient accept by perid: 46773";
        $sql = "INSERT INTO OPD.HOME_APPOINT (
                    HN,
                    PNAME,
                    DATE_AP,
                    TIME_AP,
                    UNIT_AP,
                    CODE_AP,
                    DATE_REG,
                    WARD_REG,
                    DOCT_REG,
                    C_DOCT,
                    PRN_ST,
                    FR_DOCT,
                    `COMMENT`,
                    PHONE
                )
                VALUES
                    ('" . $hn . "',
                    '" . $pname . "',
                    '" . $dateap . "',
                    '" . $timeap . "',
                    '" . $unitap . "',
                    '" . $codeap . "',
                    '" . $datereg . "',
                    '" . $wardreg . "',
                    '" . $doctreg . "',
                    '" . $cdoct . "',
                    '" . $prnst . "',
                    '" . $frdoct . "',
                    '" . $comment . "',
                    '" . $phone . "') -- Insert mydata appoint by perid: 46773";
        $sql = $this->UTF8_TO_TIS620($sql);
        $this->Query($sql);
    }

    public function CheckPaymentHistory($data)
    {
        $pdo = new PDO($this->dsn, $this->user, $this->passwd);
        $hn = $data->hn;
        // $sql = "SELECT * FROM OPD.HOME_MONEY WHERE hn = '" . $hn . "' AND (transaction_id IS NULL OR transaction_id = 0) AND (ref1 IS NULL OR ref1 = '') AND (ref2 IS NULL OR ref2 = '') -- Select OPD.HOME_MONEY by perid: 46773";
        $sql = "SELECT * FROM OPD.HOME_MONEY WHERE hn = '" . $hn . "' AND (transaction_id IS NULL OR transaction_id = 0) -- Select OPD.HOME_MONEY by perid: 46773";
        $stm = $pdo->prepare($sql);
        $stm->execute();
        $data = $stm->fetchAll(PDO::FETCH_ASSOC);
        $allData = array(
            "statusCode" => 200,
            "data" => $data,
        );
        echo json_encode($allData);
    }

    public function AllMoney($data)
    {
        $pdo = new PDO($this->dsn, $this->user, $this->passwd);
        $hn = $data->hn;
        $invoicedate = $data->invoicedate;
        $invoiceid = $data->invoiceid;
        $sql = "SELECT HN, SENDDATE, SUM(MONEY) AS MONEY, PAY, InvoiceId FROM OPD.Allmoney WHERE HN = '" . $hn . "' AND InvoiceId = '" . $invoiceid . "' AND (OLD_REF <> 'DDDD' OR OLD_REF IS NULL) -- Select patient accept by perid: 46773";
        // $sql = "SELECT HN, SENDDATE, MONEY, PAY, InvoiceId FROM OPD.Allmoney WHERE HN = '" . $hn . "' AND InvoiceId = '" . $invoiceid . "' AND (OLD_REF <> 'DDDD' OR OLD_REF IS NULL) -- Select patient accept by perid: 46773";
        $stm = $pdo->prepare($sql);
        $stm->execute();
        $data = $stm->fetchAll(PDO::FETCH_ASSOC);
        $allData = array(
            "statusCode" => 200,
            "data" => $data,
        );
        echo json_encode($allData);
    }

    public function HomeMoney($data)
    {
        $pdo = new PDO($this->dsn, $this->user, $this->passwd);
        $hn = $data->hn;
        $invoicedate = $data->invoicedate;
        $invoiceid = $data->invoiceid;
        $sql = "SELECT * FROM OPD.HOME_MONEY WHERE hn = '" . $hn . "' AND invoicedate = '" . $invoicedate . "' AND invoiceno = '" . $invoiceid . "' -- Select patient accept by perid: 46773";
        $stm = $pdo->prepare($sql);
        $stm->execute();
        $data = $stm->fetchAll(PDO::FETCH_ASSOC);
        $allData = array(
            "statusCode" => 200,
            "data" => $data,
        );
        echo json_encode($allData);
    }

    public function QRPayment($data)
    {
        // $url = "https://564c14593d85.ngrok.io/api/qrcodepayment";
        $url = "http://61.19.201.20:19539/medservice/qrcodepayment";
        $ch = curl_init($url);
        $payload = json_encode($data);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);
        echo $result;
    }

    // *Step 1 Update ref1 & ref2 when clicked gen qrcode
    public function UpdateHomeMoneyRef1Ref2($data)
    {
        $pdo = new PDO($this->dsn, $this->user, $this->passwd);
        $transactionid = $data->transactionid;
        $ref1 = $data->ref1;
        $ref2 = $data->ref2;
        $hn = $data->hn;
        $invoiceno = $data->invoiceno;

        $sql = "UPDATE OPD.HOME_MONEY SET ref1 = '" . $ref1 . "', ref2 = '" . $ref2 . "' WHERE hn = '" . $hn . "' AND invoiceno = '" . $invoiceno . "' AND (ref1 = '' OR ref1 IS NULL) AND (ref2 = '' OR ref2 IS NULL) -- Select patient accept by perid: 46773";
        $this->Query($sql);
    }

    // *Step 2 Check Transaction data
    public function ReCheckPaymentTransfer($data)
    {
        $pdo = new PDO($this->dsn, $this->user, $this->passwd);
        $ref1 = $data->ref1;
        $ref2 = $data->ref2;
        $amount = $data->amount;

        $sql = "SELECT * FROM MONEY.TRANSACTIONS WHERE amount = '" . $amount . "' AND reference1 = '" . $ref1 . "' AND reference2 = '" . $ref2 . "' -- Select TRANSACTIONS accept by perid: 46773";
        $this->Query($sql);
    }

    // *Step 3 If transfer success update the transaction_id in HOME_MONEY
    public function UpdateHomeMoney($data)
    {
        $pdo = new PDO($this->dsn, $this->user, $this->passwd);
        $transactionid = $data->transactionid;
        $ref1 = $data->ref1;
        $ref2 = $data->ref2;
        $hn = $data->hn;
        $invoiceno = $data->invoiceno;

        $sql = "UPDATE OPD.HOME_MONEY SET transaction_id = '" . $transactionid . "' WHERE hn = '" . $hn . "' AND invoiceno = '" . $invoiceno . "' AND ref1 = '" . $ref1 . "' AND ref2 = '" . $ref2 . "' -- Select patient accept by perid: 46773";
        $this->Query($sql);
    }

    public function PatientAddress($data)
    {
        $pdo = new PDO($this->dsn, $this->user, $this->passwd);
        $phone = $data->phone;
        $hn = $data->hn;
        $sql = "SELECT hn, names, address, address2, address3, changwat, amphur, tumbon, postal, phone, sdatetime, sperid  FROM OPD.HOME_ADDRESS_HN WHERE HN = '" . $hn . "' AND phone LIKE '" . $phone . "' LIMIT 1 -- Select patient address by perid: 46773";
        $stm = $pdo->prepare($sql);
        $stm->execute();
        $data = $stm->fetchAll(PDO::FETCH_ASSOC);
        $allData = array(
            "statusCode" => 200,
            "data" => $data,
        );
        echo json_encode($allData);
    }

    public function PatientAddressMedrec($data)
    {
        $pdo = new PDO($this->dsn, $this->user, $this->passwd);
        $phone = $data->phone;
        $hn = $data->hn;
        $sql = "SELECT HN, PINITIAL, PNAME, PSUR, RPHONE, PADDRESS, PAREA FROM Mydata.Medrec WHERE HN = '" . $hn . "' AND RPHONE LIKE '" . $phone . "' LIMIT 1 -- Select patient address by perid: 46773";
        $stm = $pdo->prepare($sql);
        $stm->execute();
        $data = $stm->fetchAll(PDO::FETCH_ASSOC);
        $allData = array(
            "statusCode" => 200,
            "data" => $data,
        );
        echo json_encode($allData);
    }

    public function AllChangWat()
    {
        $pdo = new PDO($this->dsn, $this->user, $this->passwd);
        $sql = "SELECT CWAT, NWAT FROM Mydata.Fwat -- Select all changwat by perid: 46773";
        $stm = $pdo->prepare($sql);
        $stm->execute();
        $data = $stm->fetchAll(PDO::FETCH_ASSOC);
        $allData = array(
            "statusCode" => 200,
            "data" => $data,
        );
        echo json_encode($allData);
    }

    public function AllAmphur($data)
    {
        $pdo = new PDO($this->dsn, $this->user, $this->passwd);
        $cwat = $data->cwat;
        $sql = "SELECT CWAT, CPHUR, NPHUR FROM Mydata.Famphur WHERE CWAT = '" . $cwat . "' -- Select all changwat by perid: 46773";
        $stm = $pdo->prepare($sql);
        $stm->execute();
        $data = $stm->fetchAll(PDO::FETCH_ASSOC);
        $allData = array(
            "statusCode" => 200,
            "data" => $data,
        );
        echo json_encode($allData);
    }

    public function AllTumbon($data)
    {
        $pdo = new PDO($this->dsn, $this->user, $this->passwd);
        $cwatphur = $data->cwatphur;
        $sql = "SELECT CWATPHUR, CBON, NBON, CPOST FROM Mydata.Ftumbon WHERE CWATPHUR = '" . $cwatphur . "' -- Select all changwat by perid: 46773";
        $stm = $pdo->prepare($sql);
        $stm->execute();
        $data = $stm->fetchAll(PDO::FETCH_ASSOC);
        $allData = array(
            "statusCode" => 200,
            "data" => $data,
        );
        echo json_encode($allData);
    }

    public function InsertAddressConfirm($data)
    {
        $hn = $data->hn;
        $names = $data->names;
        $address = $data->address;
        $address2 = $data->address2;
        $address3 = $data->address3;
        $changwat = $data->changwat;
        $amphur = $data->amphur;
        $tumbon = $data->tumbon;
        $postal = $data->postal;
        $phone = $data->phone;
        $sql = "INSERT INTO OPD.HOME_ADDRESS_HN(hn, names, address, address2, address3, changwat, amphur, tumbon, postal, phone)
                VALUES(
                    '" . $hn . "',
                    '" . $names . "',
                    '" . $address . "',
                    '" . $address2 . "',
                    '" . $address3 . "',
                    '" . $changwat . "',
                    '" . $amphur . "',
                    '" . $tumbon . "',
                    '" . $postal . "',
                    '" . $phone . "') -- INSERT patient address by perid: 46773";
        $sql = $this->UTF8_TO_TIS620($sql);
        $this->Query($sql);
    }

    public function UpdateAddressConfirm($data)
    {
        $hn = $data->hn;
        $names = $data->names;
        $address = $data->address;
        $address2 = $data->address2;
        $address3 = $data->address3;
        $changwat = $data->changwat;
        $amphur = $data->amphur;
        $tumbon = $data->tumbon;
        $postal = $data->postal;
        $sql = "UPDATE OPD.HOME_ADDRESS_HN
                SET names = '" . $names . "',
                    address = '" . $address . "',
                    address2 = '" . $address2 . "',
                    address3 = '" . $address3 . "',
                    changwat = '" . $changwat . "',
                    amphur = '" . $amphur . "',
                    tumbon = '" . $tumbon . "',
                    postal = '" . $postal . "'
                WHERE hn = '" . $hn . "' -- UPDATE patient address by perid: 46773";
        $sql = $this->UTF8_TO_TIS620($sql);
        $this->Query($sql);
    }

    public function UpdatePatientAccepted($data)
    {
        $hn = $data->hn;
        $accept = $data->accept;
        $dateap = $data->dateap;
        $codeap = $data->codeap;
        $cdoct = $data->cdoct;
        $sql = "UPDATE OPD.HOME_APPOINT
        SET PATIENT_ACCEPT = '" . $accept . "',
        DATE_ACCEPT = CURDATE()
        WHERE HN = '" . $hn . "' and DATE_AP = '" . $dateap . "' and CODE_AP = '" . $codeap . "' and C_DOCT = '" . $cdoct . "' -- UPDATE patient accept address by perid: 46773";
        $this->Query($sql);
    }

    public function DMDataSaveOnline($data)
    {
        $url = "http://61.19.201.20:19539/medservice/callapisaveonline";

        $ch = curl_init($url);
        $payload = json_encode($data);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);
        echo $result;
    }

    public function UpdateDMApproveID($data)
    {
        $hn = $data->hn;
        $invoiceid = $data->invoiceid;
        $ref2 = $data->ref2;
        $sql = "UPDATE OPDMONEY.Dm_ApproveID SET VerifyRefID = '" . $ref2 . "' WHERE HN = '" . $hn . "' AND InvoiceID = '" . $invoiceid . "' -- Update VerifyRefID";
        $sql = $this->UTF8_TO_TIS620($sql);

        $logjsondata = array(
            "date" => date("Y-m-d h:i:s"),
            "hn" => $hn,
            "invoiceid" => $invoiceid,
            "ref2" => $ref2,
            "sql" => $sql,
        );
        $path = __DIR__ . "/../logs/dmapproveid/log-dmapproveid-" . date("Y-m-d") . ".txt";
        $newRequestData = date("Y-m-d H:i:s") . "\tdata: " . trim(preg_replace('/\s/', '', json_encode($logjsondata))) . PHP_EOL;
        @file_put_contents($path, $newRequestData, FILE_APPEND);

        $this->Query($sql);
        // $pdo = new PDO($this->dsn, $this->user, $this->passwd);
        // $stm = $pdo->prepare($sql);
        // $stm->execute();
        // $data = $stm->fetchAll(PDO::FETCH_ASSOC);
        // $allData = array(
        //     "statusCode" => 200,
        //     "message" => $data,
        // );
        // echo json_encode($allData);
    }

    public function SCBBNLastUpdate()
    {
        $pdo = new PDO($this->dsn, $this->user, $this->passwd);
        $sql = "UPDATE
                    MONEY.SCBBN_CONFIGS
                SET
                    `value` = CONCAT(CURDATE(),' ',CURTIME())
                WHERE
                    `key` = 'LAST_UPDATE'  -- Update SCBBN_LAST_UPDATE";
        $stm = $pdo->prepare($sql);
        $stm->execute();
        $data = $stm->fetchAll(PDO::FETCH_ASSOC);
        $allData = array(
            "statusCode" => 200,
            "data" => $data,
        );
        echo json_encode($allData);
    }

    public function TrackingDrugs($data)
    {
        $pdo = new PDO($this->dsn, $this->user, $this->passwd);
        $hn = $data->hn;
        $dateap = $data->dateap;
        $cdoct = $data->cdoct;
        $codeap = $data->codeap;
        $sql = "SELECT * FROM OPD.HOME_DRUG_DETAIL WHERE HN = '" . $hn . "' AND date_ap = '" . $dateap . "' AND c_doct = '" . $cdoct . "' AND code_ap = '" . $codeap . "' AND (tracking_no <> '' AND tracking_no IS NOT NULL) -- Select tracking number by perid: 46773";
        $stm = $pdo->prepare($sql);
        $stm->execute();
        $data = $stm->fetchAll(PDO::FETCH_ASSOC);
        $allData = array(
            "statusCode" => 200,
            "data" => $data,
        );
        echo json_encode($allData);
    }

    public function Query($sql)
    {
        $sql = urlencode($sql);
        // 192.168.1.33 for HIS-Real
        $url = "http://192.168.1.33/his/gen12c.php?sql=" . $sql;
        // 172.29.1.4 for HIS-Test
        // $url = "http://172.29.1.4/his-test/gen12c.php?sql=" . $sql;

        $header = array(
            "http" => [
                "method" => "GET",
                "header" => "User-Agent: Microsoft\r\n",
            ],
        );
        $context = stream_context_create($header);
        $result = iconv("TIS-620", "UTF-8", file_get_contents($url, false, $context));
        // $result = file_get_contents($url, false, $context);
        $head = array();
        foreach ($http_response_header as $k => $v) {
            $t = explode(':', $v, 2);
            if (isset($t[1])) {
                $head[trim($t[0])] = trim($t[1]);
            } else {
                $head[] = $v;
                if (preg_match("#HTTP/[0-9\.]+\s+([0-9]+)#", $v, $out)) {
                    $head['reponse_code'] = intval($out[1]);
                }
            }
        }
        $columns = explode("\\u0001", $head["Columns"]);
        $data = explode(chr(2), $result);
        $table = array();
        foreach ($data as $v) {
            if (strlen($v) > 0) {
                $line = explode(chr(1), $v);
                $itemArr = array();
                foreach ($columns as $index => $name) {
                    $itemArr[$name] = $line[$index];
                }
                array_push($table, $itemArr);
            }
        }

        header("Content-type:application/json");
        $jsondata = json_encode($table);
        $result = array(
            "statusCode" => 200,
            "data" => $jsondata,
        );
        echo json_encode($result);
    }

    public function UTF8_TO_TIS620($sql)
    {
        return iconv("UTF-8", "TIS-620", $sql);
    }

    public function TIS620_TO_UTF8($sql)
    {
        return iconv("TIS-620", "UTF-8", $sql);
    }
}
